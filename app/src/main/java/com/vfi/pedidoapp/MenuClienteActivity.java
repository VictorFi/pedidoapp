package com.vfi.pedidoapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.vfi.pedidoapp.adapter.PedidoAdapter;
import com.vfi.pedidoapp.base.Pedido;
import com.vfi.pedidoapp.basedatos.BaseDatos;

import java.util.ArrayList;

public class MenuClienteActivity extends AppCompatActivity implements View.OnClickListener {

    private PedidoAdapter adapter;
    private ArrayList<Pedido> listaPedidos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_cliente);

        listaPedidos = new ArrayList<>();
        ListView lvPedidos = (ListView) findViewById(R.id.lvPedidosC);
        registerForContextMenu(lvPedidos);
        adapter = new PedidoAdapter(this,listaPedidos);
        lvPedidos.setAdapter(adapter);

        Button bCrearPedido = (Button) findViewById(R.id.bCrearPedido);
        bCrearPedido.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if(v.getId()==R.id.bCrearPedido){
            intent = new Intent(this,CrearPedidoActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        listaPedidos.clear();
        BaseDatos db= new BaseDatos(this);
        listaPedidos.addAll(db.obtenerPedidos());
        Toast.makeText(this, R.string.lista+": "+listaPedidos.size(), Toast.LENGTH_SHORT).show();
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.acerca_de:
                Intent intent = new Intent(this,AcercaDeActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menucliente_context,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int posicion = menuInfo.position;

        Pedido pedido = null;
        switch (item.getItemId()){
            case R.id.menu_modificar:
                Intent intent = new Intent(this,CrearPedidoActivity.class);
                pedido = listaPedidos.get(posicion);
                intent.putExtra("pedido",pedido);
                startActivity(intent);
                return true;
            case R.id.menu_eliminar:
                pedido = listaPedidos.remove(posicion);
                BaseDatos db = new BaseDatos(this);
                db.eliminarProducto(pedido);
                adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }
}
