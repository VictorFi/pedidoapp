package com.vfi.pedidoapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.vfi.pedidoapp.R;
import com.vfi.pedidoapp.adapter.PedidoAdapter;
import com.vfi.pedidoapp.base.Pedido;
import com.vfi.pedidoapp.basedatos.BaseDatos;

import java.util.ArrayList;

public class MenuTrabajadorActivity extends AppCompatActivity implements View.OnClickListener {

    private PedidoAdapter adapter;
    private ArrayList<Pedido> listaPedidos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_trabajador);

        listaPedidos = new ArrayList<>();
        ListView lvPedidos = (ListView) findViewById(R.id.lvPedidosT);
        registerForContextMenu(lvPedidos);
        adapter = new PedidoAdapter(this,listaPedidos);
        lvPedidos.setAdapter(adapter);

        Button bMapa = (Button) findViewById(R.id.bMapa);
        bMapa.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if(v.getId()==R.id.bMapa){
            intent = new Intent(this,MapaActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        listaPedidos.clear();
        BaseDatos db= new BaseDatos(this);
        listaPedidos.addAll(db.obtenerPedidos());
        Toast.makeText(this, R.string.lista+": "+listaPedidos.size(), Toast.LENGTH_SHORT).show();
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.acerca_de:
                Intent intent = new Intent(this,AcercaDeActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
