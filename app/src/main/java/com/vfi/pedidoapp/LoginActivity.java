package com.vfi.pedidoapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button bTrabajador = (Button) findViewById(R.id.bTrabajador);
        bTrabajador.setOnClickListener(this);
        Button bCliente = (Button) findViewById(R.id.bCliente);
        bCliente.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if(v.getId() == R.id.bTrabajador){
            intent = new Intent(this,RegistroTrabajadorActivity.class);
            startActivity(intent);
        }else if(v.getId() == R.id.bCliente){
            intent = new Intent(this,RegistroUsuarioActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.acerca_de:
                Intent intent = new Intent(this,AcercaDeActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
