package com.vfi.pedidoapp;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.vfi.pedidoapp.base.CentroComercial;
import com.vfi.pedidoapp.base.Pedido;
import com.vfi.pedidoapp.basedatos.BaseDatos;
import com.vfi.pedidoapp.util.Constantes;
import com.vfi.pedidoapp.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MapaActivity extends AppCompatActivity {

    private MapView mapaView;
    private MapboxMap mapa;
    private LocationServices servicioUbicacion;
    private ArrayList<CentroComercial> listaCentrosComerciales;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapboxAccountManager.start(this,"pk.eyJ1IjoidmljdG9yZmkiLCJhIjoiY2phOWJkNmhvMGk3czJ3anV3aHFtamtsNyJ9.WbcLwqov-5CzHbKbyTIlWQ");
        setContentView(R.layout.activity_mapa);

        listaCentrosComerciales = new ArrayList<>();

        mapaView = (MapView) findViewById(R.id.mapaView);
        mapaView.onCreate(savedInstanceState);
        mapaView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapa = mapboxMap;
            }
        });

        DescargaDatos tarea = new DescargaDatos();
        tarea.execute();
    }

    private void ubicarUsuario(){

        servicioUbicacion = LocationServices.getLocationServices(this);
        if(mapa != null){
            Location lastLocation = servicioUbicacion.getLastLocation();
            if(lastLocation != null){
                mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation),16));

            }
        }
        mapa.setMyLocationEnabled(true);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menumapa_context,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_puntos_interes:
                marcadores();
                return true;
            case R.id.donde_estoy:
                ubicarUsuario();
                return true;
            case R.id.acerca_de:
                Intent intent = new Intent(this,AcercaDeActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void marcadores(){
        for(CentroComercial c: listaCentrosComerciales){
           final MarkerOptions markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng();
            latLng.setLatitude((Util.DeUMTSaLatLng(c.getLatitud(),c.getLongitud())).getLat());
            latLng.setLongitude((Util.DeUMTSaLatLng(c.getLatitud(),c.getLongitud())).getLng());
            markerOptions.setPosition(latLng);
            markerOptions.setTitle(c.getTitulo());
            markerOptions.setSnippet(c.getCalle());
            mapaView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(MapboxMap mapboxMap) {
                    mapboxMap.addMarker(markerOptions);
                }
            });
        }
    }
    private class DescargaDatos extends AsyncTask<Void,Void,Void>{

        String resultado = null;
        InputStream is = null;
        JSONObject json = null;
        JSONArray jsonArray = null;
        JSONArray jsonArrayCentroComercial = null;
        boolean error = false;

        @Override
        protected Void doInBackground(Void... params) {
            try{
                URL url = new URL(Constantes.URL);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String linea = null;

                while((linea = br.readLine()) != null){
                    sb.append(linea +"\n");
                }

                conexion.disconnect();
                br.close();
                resultado = sb.toString();

                json = new JSONObject(resultado);
                jsonArray = json.getJSONArray("result");

                String titulo;
                String servicio;
                String calle;
                String coordenadas;
                CentroComercial centroComercial = null;

                for(int i=0;i<jsonArray.length();i++){
                    if(jsonArray.getJSONObject(i).getString("title").contains("Centro Comercial")){
                        jsonArrayCentroComercial = jsonArray.getJSONObject(i).getJSONArray("equipamiento");
                        for(int j=0;j<jsonArrayCentroComercial.length();j++){
                            if(jsonArrayCentroComercial.getJSONObject(j).has("servicios") && jsonArrayCentroComercial.getJSONObject(j).has("geometry")){

                                titulo = jsonArrayCentroComercial.getJSONObject(j).getString("title");
                                servicio = jsonArrayCentroComercial.getJSONObject(j).getString("servicios");
                                calle = jsonArrayCentroComercial.getJSONObject(j).getString("calle");
                                coordenadas = jsonArrayCentroComercial.getJSONObject(j).getJSONObject("geometry").getString("coordinates");
                                coordenadas = coordenadas.substring(1,coordenadas.length() - 1);
                                String latlong[] = coordenadas.split(",");

                                Log.d("titulo",titulo);
                                Log.d("servicio",servicio);
                                Log.d("calle",calle);
                                Log.d("coordenadas",latlong[0]+"/"+latlong[1]);
                                centroComercial = new CentroComercial();
                                centroComercial.setTitulo(titulo);
                                centroComercial.setCalle(calle);
                                centroComercial.setServicio(servicio);
                                centroComercial.setLatitud(Float.parseFloat(latlong[0]));
                                centroComercial.setLongitud(Float.parseFloat(latlong[1]));
                                listaCentrosComerciales.add(centroComercial);
                            }
                        }

                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();

            } catch (JSONException e) {
                e.printStackTrace();
                error = true;
            }
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            listaCentrosComerciales = new ArrayList<>();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getApplicationContext(),R.string.cargando,Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onPostExecute(Void resultado) {
            super.onPostExecute(resultado);
            if(error){
                Toast.makeText(getApplicationContext(),R.string.error,Toast.LENGTH_LONG).show();
                return;
            }else{
                Toast.makeText(getApplicationContext(),R.string.cargado,Toast.LENGTH_LONG).show();
                return;
            }
        }
    }
}
