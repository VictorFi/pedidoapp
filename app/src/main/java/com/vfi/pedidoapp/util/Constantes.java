package com.vfi.pedidoapp.util;

/**
 * Created by PC on 12/12/2017.
 */

public class Constantes {
    public static final String TABLA_TRABAJADORES = "trabajadores";
    public static final String USUARIO = "usuario";
    public static final String CONTRASENA = "contrasena";
    public static final String NOMBRE = "nombre";
    public static final String APELLIDOS = "apellidos";
    public static final String DNI = "dni";
    public static final String MOTO = "moto";
    public static final String TABLA_USUARIOS = "usuarios";
    public static final String CREDITO = "credito";
    public static final String DIRECCION = "direccion";
    public static final String ID = "id";
    public static final String TABLA_PRODUCTOS = "productos";
    public static final String PRODUCTO = "producto";
    public static final String CANTIDAD = "cantidad";
    public static final String FECHA = "fecha";
    public static final String URL = "https://www.zaragoza.es/sede/servicio/equipamiento/category.json";
    public static final String URL_SERVIDOR =  "http://10.0.2.2:8080";
}
