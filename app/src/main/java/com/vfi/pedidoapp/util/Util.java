package com.vfi.pedidoapp.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;


/**
 * Created by PC on 15/12/2017.
 */

public class Util {
    public static Date parseFecha(String fecha) throws ParseException {
        DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
        return df.parse(fecha);
    }
    public static String formatFecha(Date fecha){
        DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
        return df.format(fecha);
    }
    public static LatLng DeUMTSaLatLng(double latitud, double longitud){
        UTMRef utm = new UTMRef(latitud,longitud,'N',30);
        return utm.toLatLng();
    }
}
