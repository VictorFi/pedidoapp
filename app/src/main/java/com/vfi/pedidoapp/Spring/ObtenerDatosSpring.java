package com.vfi.pedidoapp.Spring;

import android.os.AsyncTask;
import android.util.Log;

import com.vfi.pedidoapp.base.Pedido;
import com.vfi.pedidoapp.base.Trabajador;
import com.vfi.pedidoapp.base.Usuario;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;

import static com.vfi.pedidoapp.util.Constantes.URL_SERVIDOR;

/**
 * Created by PC on 05/01/2018.
 */

public class ObtenerDatosSpring extends AsyncTask<String,Void,Void> {
    private ArrayList<Trabajador> listaTrabajadores;
    private ArrayList<Usuario> listaClientes;
    private ArrayList<Pedido> listaPedidos;
    private String URL_Servidor = URL_SERVIDOR;
    private RestTemplate restTemplate;
    @Override
    protected Void doInBackground(String... params) {
        restTemplate = new RestTemplate();

        listaTrabajadores = new ArrayList<>();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Trabajador[] trabajadorArray = restTemplate.getForObject(URL_Servidor + "/trabajadores", Trabajador[].class);
        listaTrabajadores.addAll(Arrays.asList(trabajadorArray));

        listaClientes = new ArrayList<>();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Usuario[] usuarioArray = restTemplate.getForObject(URL_Servidor + "/clientes", Usuario[].class);
        listaClientes.addAll(Arrays.asList(usuarioArray));

        listaPedidos = new ArrayList<>();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Pedido[] pedidoArray = restTemplate.getForObject(URL_Servidor + "/pedidos", Pedido[].class);
        listaPedidos.addAll(Arrays.asList(pedidoArray));

        return null;
    }
    public boolean obtenerTrabajador(String usuario,String contrasena){
        Log.d("nombre",listaTrabajadores.get(0).getNombre());
        boolean valido = false;
        for(Trabajador t:listaTrabajadores){
            if(t.getUsuario().equals(usuario) && t.getContrasena().equals(contrasena)){
                valido = true;
                break;
            }
        }
        return valido;
    }
    public boolean obtenerCliente(String usuario,String contrasena){
        boolean valido = false;
        for(Usuario u:listaClientes){
            if(u.getUsuario().equals(usuario) && u.getContrasena().equals(contrasena)){
                valido = true;
                break;
            }
        }
        return valido;
    }
    public ArrayList<Trabajador> getListaTrabajadores() {
        return listaTrabajadores;
    }

    public ArrayList<Usuario> getListaClientes() {
        return listaClientes;
    }

    public ArrayList<Pedido> getListaPedidos() {
        return listaPedidos;
    }
}
