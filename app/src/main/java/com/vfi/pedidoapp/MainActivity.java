package com.vfi.pedidoapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vfi.pedidoapp.Spring.ObtenerDatosSpring;
import com.vfi.pedidoapp.basedatos.BaseDatos;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    EditText nombre;
    EditText contrasena;
    ObtenerDatosSpring datosSpring;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        datosSpring = new ObtenerDatosSpring();
        datosSpring.execute();
        Button bEntrar = (Button) findViewById(R.id.bEntrar);
        bEntrar.setOnClickListener(this);
        Button bCrear = (Button) findViewById(R.id.bCrear);
        bCrear.setOnClickListener(this);
        nombre = (EditText) findViewById(R.id.etUsuario);
        contrasena = (EditText) findViewById(R.id.etContrasena);


    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if(v.getId()==R.id.bEntrar){
            BaseDatos baseDatos = new BaseDatos(this);
            /*if(baseDatos.obtenerTrabajador(nombre.getText().toString(),contrasena.getText().toString())){
                intent = new Intent(this,MenuTrabajadorActivity.class);
                Toast.makeText(this,"Trabajador Valido",Toast.LENGTH_LONG).show();
                startActivity(intent);
            }else if(baseDatos.obtenerCliente(nombre.getText().toString(),contrasena.getText().toString())){
                intent = new Intent(this,MenuClienteActivity.class);
                Toast.makeText(this,"Cliente Valido",Toast.LENGTH_LONG).show();
                startActivity(intent);
            }*/
            if(datosSpring.obtenerTrabajador(nombre.getText().toString(),contrasena.getText().toString())){
                intent = new Intent(this,MenuTrabajadorActivity.class);
                Toast.makeText(this,R.string.trabajadorValido,Toast.LENGTH_LONG).show();
                startActivity(intent);
            }else if(datosSpring.obtenerCliente(nombre.getText().toString(),contrasena.getText().toString())){
                intent = new Intent(this,MenuClienteActivity.class);
                Toast.makeText(this,R.string.clienteValido,Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        }else if(v.getId()==R.id.bCrear){
            intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.acerca_de:
                Intent intent = new Intent(this,AcercaDeActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
