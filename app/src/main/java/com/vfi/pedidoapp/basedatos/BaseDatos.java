package com.vfi.pedidoapp.basedatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.vfi.pedidoapp.base.Pedido;
import com.vfi.pedidoapp.base.Trabajador;
import com.vfi.pedidoapp.base.Usuario;
import com.vfi.pedidoapp.util.Util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import static com.vfi.pedidoapp.util.Constantes.APELLIDOS;
import static com.vfi.pedidoapp.util.Constantes.CANTIDAD;
import static com.vfi.pedidoapp.util.Constantes.CONTRASENA;
import static com.vfi.pedidoapp.util.Constantes.CREDITO;
import static com.vfi.pedidoapp.util.Constantes.DIRECCION;
import static com.vfi.pedidoapp.util.Constantes.DNI;
import static com.vfi.pedidoapp.util.Constantes.FECHA;
import static com.vfi.pedidoapp.util.Constantes.ID;
import static com.vfi.pedidoapp.util.Constantes.MOTO;
import static com.vfi.pedidoapp.util.Constantes.NOMBRE;
import static com.vfi.pedidoapp.util.Constantes.PRODUCTO;
import static com.vfi.pedidoapp.util.Constantes.TABLA_PRODUCTOS;
import static com.vfi.pedidoapp.util.Constantes.TABLA_TRABAJADORES;
import static com.vfi.pedidoapp.util.Constantes.TABLA_USUARIOS;
import static com.vfi.pedidoapp.util.Constantes.USUARIO;

/**
 * Created by PC on 12/12/2017.
 */

public class BaseDatos extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "pedidoapp2.db";
    private static final int DATABASE_VERSION = 1;

    public BaseDatos(Context contexto){
        super(contexto,DATABASE_NAME,null,DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+ TABLA_TRABAJADORES + "("+"id INTEGER PRIMARY KEY AUTOINCREMENT,"+USUARIO+" TEXT,"+CONTRASENA+" TEXT,"+NOMBRE+" TEXT,"+APELLIDOS+" TEXT,"+DNI+" TEXT,"+MOTO+" BOOLEAN)");
        db.execSQL("CREATE TABLE "+ TABLA_USUARIOS + "("+"id INTEGER PRIMARY KEY AUTOINCREMENT,"+USUARIO+" TEXT,"+CONTRASENA+" TEXT,"+NOMBRE+" TEXT,"+CREDITO+" TEXT,"+DIRECCION+" TEXT)");
        db.execSQL("CREATE TABLE "+ TABLA_PRODUCTOS + "("+"id INTEGER PRIMARY KEY AUTOINCREMENT,"+PRODUCTO+" TEXT,"+CANTIDAD+" INT,"+DIRECCION+" TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE "+TABLA_TRABAJADORES);
        db.execSQL("DROP TABLE "+TABLA_USUARIOS);
        db.execSQL("DROP TABLE "+TABLA_PRODUCTOS);
        onCreate(db);
    }
    public void insertarTrabajador(Trabajador trabajador){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(USUARIO,trabajador.getUsuario());
        valores.put(CONTRASENA,trabajador.getContrasena());
        valores.put(NOMBRE,trabajador.getNombre());
        valores.put(APELLIDOS,trabajador.getApellidos());
        valores.put(DNI,trabajador.getDni());
        valores.put(MOTO,trabajador.isMoto());

        db.insertOrThrow(TABLA_TRABAJADORES,null,valores);
    }
    public void insertarUsuario(Usuario usuario){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(USUARIO,usuario.getUsuario());
        valores.put(CONTRASENA,usuario.getContrasena());
        valores.put(NOMBRE,usuario.getNombre());
        valores.put(CREDITO,usuario.getCredito());
        valores.put(DIRECCION,usuario.getDireccion());

        db.insertOrThrow(TABLA_USUARIOS,null,valores);
    }
    public void insertarPedido(Pedido pedido){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(PRODUCTO,pedido.getProducto());
        valores.put(CANTIDAD,pedido.getCantidad());
        valores.put(DIRECCION, pedido.getDireccion());

        db.insertOrThrow(TABLA_PRODUCTOS,null,valores);
    }
    public ArrayList<Trabajador> obtenerTrabajadores(){
        ArrayList<Trabajador> listaTrabajadores = new ArrayList<>();
        final String[] SELECT ={ID,USUARIO,CONTRASENA,NOMBRE,APELLIDOS,DNI,MOTO};
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLA_TRABAJADORES,SELECT,null,null,null,null,null);
        while (cursor.moveToNext()){
            Trabajador trabajador = new Trabajador();
            trabajador.setId(cursor.getInt(0));
            trabajador.setUsuario(cursor.getString(1));
            trabajador.setContrasena(cursor.getString(2));
            trabajador.setNombre(cursor.getString(3));
            trabajador.setApellidos(cursor.getString(4));
            trabajador.setDni(cursor.getString(5));
            if(cursor.getInt(6)==0){
                trabajador.setMoto(false);
            }else{
                trabajador.setMoto(true);
            }
            listaTrabajadores.add(trabajador);
        }
        return listaTrabajadores;
    }
    public boolean obtenerTrabajador(String usuario,String contrasena){
        boolean valido = false;
        ArrayList<Trabajador> listaTrabajadores = obtenerTrabajadores();
            for(Trabajador t:listaTrabajadores){
                if(t.getUsuario().equals(usuario) && t.getContrasena().equals(contrasena)){
                    valido = true;
                    break;
                }
            }
        return valido;
    }
    public ArrayList<Usuario> obtenerUsuarios(){
        ArrayList<Usuario> listaUsuarios = new ArrayList<>();
        final String[] SELECT ={ID,USUARIO,CONTRASENA,NOMBRE,CREDITO,DIRECCION};
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLA_USUARIOS,SELECT,null,null,null,null,null);
        while (cursor.moveToNext()){
            Usuario usuario= new Usuario();
            usuario.setId(cursor.getInt(0));
            usuario.setUsuario(cursor.getString(1));
            usuario.setContrasena(cursor.getString(2));
            usuario.setNombre(cursor.getString(3));
            usuario.setCredito(cursor.getString(4));
            usuario.setDireccion(cursor.getString(5));

            listaUsuarios.add(usuario);
        }
        return listaUsuarios;
    }
    public boolean obtenerCliente(String usuario,String contrasena){
        boolean valido = false;
        ArrayList<Usuario> listaUsuarios = obtenerUsuarios();
            for(Usuario u:listaUsuarios){
                if(u.getUsuario().equals(usuario) && u.getContrasena().equals(contrasena)){
                    valido = true;
                    break;
                }
            }
        return valido;
    }
    public ArrayList<Pedido> obtenerPedidos(){
        ArrayList<Pedido> listaPedidos = new ArrayList<>();
        final String[] SELECT = {ID,PRODUCTO,CANTIDAD,DIRECCION};
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLA_PRODUCTOS,SELECT,null,null,null,null,null);
        while (cursor.moveToNext()){
            Pedido pedido = new Pedido();
            pedido.setId(cursor.getInt(0));
            pedido.setProducto(cursor.getString(1));
            pedido.setCantidad(cursor.getInt(2));
            pedido.setDireccion(cursor.getString(3));
            listaPedidos.add(pedido);
        }
        return listaPedidos;
    }
    public void modificarProducto(Pedido pedido){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(PRODUCTO,pedido.getProducto());
        valores.put(CANTIDAD,pedido.getCantidad());
        valores.put(DIRECCION,pedido.getDireccion());

        String[] argumentos = {String.valueOf(pedido.getId())};
        db.update(TABLA_PRODUCTOS,valores,"id = ?",argumentos);
        db.close();
    }
    public void eliminarProducto(Pedido pedido){
        SQLiteDatabase db = getWritableDatabase();

        String[] argumentos = new String[]{String.valueOf(pedido.getId())};
        db.delete(TABLA_PRODUCTOS,"id = ?",argumentos);
        db.close();
    }

}
