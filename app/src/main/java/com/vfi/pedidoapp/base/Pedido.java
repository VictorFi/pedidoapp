package com.vfi.pedidoapp.base;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by PC on 15/12/2017.
 */

public class Pedido implements Serializable{
    int id;
    String producto;
    int cantidad;
    String direccion;

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getProducto() {
        return producto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

}
