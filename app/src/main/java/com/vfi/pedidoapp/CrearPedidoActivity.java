package com.vfi.pedidoapp;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vfi.pedidoapp.base.Pedido;
import com.vfi.pedidoapp.basedatos.BaseDatos;

public class CrearPedidoActivity extends AppCompatActivity implements View.OnClickListener {
    boolean modificar;
    int idPedido;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_pedido);

        Intent intent = getIntent();
        Pedido pedido = (Pedido) intent.getSerializableExtra("pedido");
        if(pedido!= null){
            modificar = true;
            rellenar(pedido);
        }

        Button bCrear = (Button) findViewById(R.id.bCrearP);
        bCrear.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText etProducto = (EditText) findViewById(R.id.etProductoP);
        EditText etCantidad = (EditText) findViewById(R.id.etCantidadP);
        EditText etDireccion = (EditText) findViewById(R.id.etDireccionP);

        String producto = etProducto.getText().toString();
        int cantidad = Integer.parseInt(etCantidad.getText().toString());
        String direccion = etDireccion.getText().toString();

        Pedido pedido = new Pedido();
        pedido.setProducto(producto);
        pedido.setCantidad(cantidad);
        pedido.setDireccion(direccion);

        BaseDatos bd = new BaseDatos(this);

        if(modificar){
            pedido.setId(idPedido);
            bd.modificarProducto(pedido);
            Toast.makeText(this,R.string.pedidoModificado, Toast.LENGTH_SHORT).show();
        }else {
            bd.insertarPedido(pedido);
            Toast.makeText(this,R.string.pedidoAnadido, Toast.LENGTH_SHORT).show();
        }
    }
    private void rellenar(Pedido pedido){
        EditText etProducto = (EditText) findViewById(R.id.etProductoP);
        EditText etCantidad = (EditText) findViewById(R.id.etCantidadP);
        EditText etDireccion = (EditText) findViewById(R.id.etDireccionP);

        etProducto.setText(pedido.getProducto());
        etCantidad.setText(String.valueOf(pedido.getCantidad()));
        etDireccion.setText(pedido.getDireccion());

        idPedido = pedido.getId();
    }
}
