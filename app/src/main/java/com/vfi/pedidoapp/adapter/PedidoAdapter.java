package com.vfi.pedidoapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vfi.pedidoapp.R;
import com.vfi.pedidoapp.base.Pedido;

import java.util.ArrayList;

/**
 * Created by PC on 15/12/2017.
 */

public class PedidoAdapter extends BaseAdapter{

    private ArrayList<Pedido> listaPedidos;
    private Context contexto;
    private LayoutInflater inflater;

    public PedidoAdapter(Activity contexto, ArrayList<Pedido> listaPedidos){
        this.contexto = contexto;
        this.listaPedidos = listaPedidos;
        inflater = LayoutInflater.from(contexto);
    }
    static class ViewHolder{
        TextView tvProducto;
        TextView tvCantidad;
        TextView tvDireccion;
    }

    @Override
    public View getView(int posicion, View view, ViewGroup parent) {
        ViewHolder viewHolder;

        if(view==null){
            view = inflater.inflate(R.layout.pedido,null);

            viewHolder = new ViewHolder();
            viewHolder.tvProducto = (TextView) view.findViewById(R.id.tvProductoPA);
            viewHolder.tvCantidad = (TextView) view.findViewById(R.id.tvCantidadPA);
            viewHolder.tvDireccion = (TextView) view.findViewById(R.id.tvDireccionPA);

            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }

        Pedido pedido = listaPedidos.get(posicion);
        viewHolder.tvProducto.setText(pedido.getProducto());
        viewHolder.tvCantidad.setText(String.valueOf(pedido.getCantidad()));
        viewHolder.tvDireccion.setText(pedido.getDireccion());

        return view;
    }
    @Override
    public int getCount() {
        return listaPedidos.size();
    }

    @Override
    public Object getItem(int position) {
        return listaPedidos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
