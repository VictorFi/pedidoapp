package com.vfi.pedidoapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.vfi.pedidoapp.base.Trabajador;
import com.vfi.pedidoapp.basedatos.BaseDatos;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.vfi.pedidoapp.util.Constantes.URL_SERVIDOR;

public class RegistroTrabajadorActivity extends AppCompatActivity implements View.OnClickListener{

    int idTrabajador;

    String usuario;
    String contrasena;
    String nombre;
    String apellidos;
    String dni;
    Boolean moto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_trabajador);

        Intent intent = getIntent();
        Trabajador trabajador = (Trabajador) intent.getSerializableExtra("trabajador");
        Button bAnadir = (Button) findViewById(R.id.bAnadirT);
        bAnadir.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText etUsuario = (EditText) findViewById(R.id.etUsuarioT);
        EditText etContrasena = (EditText) findViewById(R.id.etContrasenaT);
        EditText etNombre = (EditText) findViewById(R.id.etNombreT);
        EditText etApellidos = (EditText) findViewById(R.id.etApellidosT);
        EditText etDni = (EditText) findViewById(R.id.etDniT);
        CheckBox cbMoto = (CheckBox) findViewById(R.id.cbMotoT);

        usuario = etUsuario.getText().toString();
        contrasena = etContrasena.getText().toString();
        nombre = etNombre.getText().toString();
        apellidos = etApellidos.getText().toString();
        dni = etDni.getText().toString();
        moto = cbMoto.isSelected();

        Trabajador trabajador = new Trabajador();
        trabajador.setUsuario(usuario);
        trabajador.setContrasena(contrasena);
        trabajador.setNombre(nombre);
        trabajador.setApellidos(apellidos);
        trabajador.setDni(dni);
        trabajador.setMoto(moto);

        BaseDatos bd = new BaseDatos(this);
        bd.insertarTrabajador(trabajador);
        InsertarTrabajadorSpring insertarTrabajadorSpring = new InsertarTrabajadorSpring();
        insertarTrabajadorSpring.execute();
        Toast.makeText(this, R.string.trabajadorAnadido, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.acerca_de:
                Intent intent = new Intent(this,AcercaDeActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private class InsertarTrabajadorSpring extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... params) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getForObject(URL_SERVIDOR + "/add_trabajador?usuario=" + usuario + "&contrasena=" + contrasena +
                    "&nombre=" + nombre + "&apellidos=" + apellidos + "&dni=" + dni + "&moto=" + moto,Void.class);
            return null;
        }
    }
}
