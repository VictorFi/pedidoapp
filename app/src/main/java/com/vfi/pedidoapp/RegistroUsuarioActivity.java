package com.vfi.pedidoapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vfi.pedidoapp.base.Usuario;
import com.vfi.pedidoapp.basedatos.BaseDatos;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.vfi.pedidoapp.util.Constantes.URL_SERVIDOR;


public class RegistroUsuarioActivity extends AppCompatActivity implements View.OnClickListener {

    String usuario;
    String contrasena;
    String nombre;
    String credito;
    String direccion;

    int idUsuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        Intent intent = getIntent();
        Usuario usuario = (Usuario) intent.getSerializableExtra("usuario");
        Button bAnadir = (Button) findViewById(R.id.bAnadirC);
        bAnadir.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText etUsuario = (EditText) findViewById(R.id.etUsuarioC);
        EditText etContrasena = (EditText) findViewById(R.id.etContrasenaC);
        EditText etNombre = (EditText) findViewById(R.id.etNombreC);
        EditText etCredito = (EditText) findViewById(R.id.etCreditoC);
        EditText etDireccion = (EditText) findViewById(R.id.etDireccionC);

        usuario = etUsuario.getText().toString();
        contrasena = etContrasena.getText().toString();
        nombre = etNombre.getText().toString();
        credito = etCredito.getText().toString();
        direccion = etDireccion.getText().toString();

        Usuario miusuario = new Usuario();
        miusuario.setUsuario(usuario);
        miusuario.setContrasena(contrasena);
        miusuario.setNombre(nombre);
        miusuario.setCredito(credito);
        miusuario.setDireccion(direccion);


        BaseDatos bd = new BaseDatos(this);
        bd.insertarUsuario(miusuario);
        Toast.makeText(this, R.string.usuarioAnadido, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.acerca_de:
                Intent intent = new Intent(this,AcercaDeActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private class InsertarTrabajadorSpring extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... params) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getForObject(URL_SERVIDOR + "/add_cliente?usuario=" + usuario + "&contrasena=" + contrasena +
                    "&nombre=" + nombre + "&credito=" + credito + "&direccion=" + direccion,Void.class);
            return null;
        }
    }
}
